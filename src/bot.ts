// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

import {
  ActionTypes,
  ActivityHandler,
  ActivityTypes,
  CardFactory,
  TurnContext
} from 'botbuilder';
import Unsplash from 'unsplash-js';

export class MyBot extends ActivityHandler {
  public unsplash;

  constructor() {
    super();
    const { APP_ACCESS_KEY, APP_SECRET_KEY } = process.env;
    this.unsplash = new Unsplash({
      applicationId: APP_ACCESS_KEY,
      secret: APP_SECRET_KEY
    });
    // See https://aka.ms/about-bot-activity-message to learn more about the message and other activity types.
    this.onMessage(async (context, next) => {
      await this.matcher(context);
      // By calling next() you ensure that the next BotHandler is run.
      await next();
    });

    this.onMembersAdded(async (context, next) => {
      const membersAdded = context.activity.membersAdded;
      for (const member of membersAdded) {
        if (member.id !== context.activity.recipient.id) {
          await context.sendActivity('Hello and welcome!');
        }
      }
      // By calling next() you ensure that the next BotHandler is run.
      await next();
    });
  }

  /**
   * =====================================
   *
   * Command control
   *
   * =====================================
   */

  private async matcher(context: TurnContext) {
    const text = context.activity.text;
    const args = text.split(' ');

    if (this.unsplash && args[0] === 'unsplash') {
      switch (args[1].toLowerCase()) {
        case 'random':
          return this.randomPhotoHandler(context);
        case 'search':
          return this.searchHandler(context, args[2]);
        default:
          break;
      }
    }
  }

  /**
   * =====================================
   *
   * Handlers
   *
   * =====================================
   */

  private async randomPhotoHandler(context: TurnContext) {
    const {
      id,
      description,
      urls: { regular }
    } = await this.getRandomPhoto();
    const reply = {
      attachments: [
        {
          contentType: 'image/*',
          contentUrl: regular,
          name: id
        }
      ],
      text: description,
      type: ActivityTypes.Message
    };

    return context.sendActivity(reply);
  }

  private async searchHandler(context: TurnContext, term: string) {
    const { results } = await this.getPhotosBySearch(term);
    const attachments = results.map(({ id, urls: { regular } }) => ({
      contentType: 'image/*',
      contentUrl: regular,
      name: id
    }));
    const reply = {
      attachments,
      type: ActivityTypes.Message
    };

    return context.sendActivity(reply);
  }

  /**
   * =====================================
   *
   * Fetchers
   *
   * =====================================
   */

  private async getRandomPhoto() {
    const response: Response = await this.unsplash.photos.getRandomPhoto();
    const photo = await response.json();
    return photo;
  }

  private async getPhotosBySearch(term: string) {
    const response: Response = await this.unsplash.search.photos(term, 1);
    const photos = await response.json();
    return photos;
  }

  /**
   * =====================================
   *
   * Utilities
   *
   * =====================================
   */
}
